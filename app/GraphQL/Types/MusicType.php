<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use App\Models\Music;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MusicType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Music',
        'description' => 'Represents a music',
        'model' => Music::class,
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::id())
            ],
            'name' => [
                'type' => Type::string()
            ],
            'shouldSkip' => [
                'type' => Type::boolean(),
                'description' => 'Control music exhibition',
            ],
        ];
    }
}
