<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use App\Models\Lyric;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class LyricType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Lyric',
        'description' => 'Represents a type',
        'model' => Lyric::class,
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::id()),
            ],
            'album_id' => [
                'type' => Type::nonNull(Type::id()),
            ],
            'music_id' => [
                'type' => Type::nonNull(Type::id()),
            ],
            'text' => [
                'type' => Type::nonNull(Type::string()),
            ],
        ];
    }
}
