<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use App\Models\Album;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class AlbumType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Album',
        'description' => 'Represents an albumn',
        'model' => Album::class,
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::id())
            ],
            'name' => [
                'type' => Type::string(),
            ],
            'artist' => [
                'type' => Type::string(),
            ],
            'duration' => [
                'type' => Type::float(),
            ],
            // 'lyrics' => [
            //     'type' => Type::listOf(GraphQL::type('Lyric')),
            //     'description' => 'List of lyrics for the album',
            //     'resolve' => function ($root, $args) {
            //         if ($args['shouldSkip']) {
            //             return null;
            //         }

            //         return $root->lyrics;
            //     },
            // ],
            // 'musics' => [
            //     'type' => Type::listOf(GraphQL::type('Music')),
            //     'description' => 'List of musics for the album',
            //     'resolve' => function ($root, $args) {
            //         if ($args['shouldSkip']) {
            //             return null;
            //         }

            //         return $root->musics;
            //     },
            // ],
        ];
    }
}
