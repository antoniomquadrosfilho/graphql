<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Lyric extends Model
{
    use HasFactory, Searchable;

    protected $table = 'lyrics';
    protected $fillable = [
        'text',
    ];

    public function searchableAs(): string
    {
        return 'lyrics_index';
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array<string, mixed>
     */
    public function toSearchableArray(): array
    {
        return [
            'id' => $this->id,
            'album_id' => $this->album_id,
            'music_id' => $this->music_id,
            'text' => $this->text,
        ];
    }
}
