<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Scout\Searchable;

class Album extends Model
{
    use HasFactory, Searchable;

    protected $table = 'albums';
    protected $fillable = [
        'name',
        'artist',
        'duration'
    ];

    public function searchableAs()
    {
        return 'albums_index';
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array<string, mixed>
     */
    public function toSearchableArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'artist' => $this->artist,
            'duration' => $this->duration,
        ];
    }

    public function lyrics(): HasMany
    {
        return $this->hasMany(Lyric::class);
    }

    public function albums(): HasMany
    {
        return $this->hasMany(Album::class);
    }
}
