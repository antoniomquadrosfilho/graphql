<?php

namespace App\Domain\Services\Repositories;

use App\Domain\Entities\ValueObjects\AlbumSearchParam;
use App\Domain\Services\Aggregates\AlbumAggregate;

interface AlbumRepository
{
  function create(AlbumAggregate $album): AlbumAggregate;
  function search(AlbumSearchParam $searchParam, array $returnedKeys): AlbumAggregate;
}
