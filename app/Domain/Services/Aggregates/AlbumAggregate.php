<?php

namespace App\Domain\Services\Aggregates;

use App\Domain\Entities\MusicCollection;
use App\Domain\Entities\Album;
use App\Domain\Entities\Music;

class AlbumAggregate
{
  public function __construct(
    private Album $album,
  ) {
  }

  function addMusic(Music $music): void
  {
    $this->album->addMusic($music);
  }

  public function getAlbum(): Album
  {
    return $this->album;
  }

  public function getMusics(): MusicCollection
  {
    return $this->album->getMusics();
  }

  function toArray(): array
  {
    return $this->album->toArray();
  }

  static function make(array $data): AlbumAggregate
  {
    return new AlbumAggregate(new Album(
      $data['id'],
      $data['name'],
      $data['artist'],
      $data['duration'],
    ));
  }
}
