<?php

namespace App\Domain\Services;

use App\Domain\Services\Aggregates\AlbumAggregate;
use App\Domain\Entities\ValueObjects\AlbumSearchParam;
use App\Domain\Services\Repositories\AlbumRepository;

class AlbumService
{
  public function __construct(private AlbumRepository $albumRepository)
  {
  }

  function create(array $data): AlbumAggregate
  {
    $album = new AlbumAggregate(
      $data['id'],
      $data['name'],
      $data['artist'],
      $data['duration']
    );

    foreach ($data['musics'] as $music) {
      $album->addMusic($music);
    }

    return $this->albumRepository->create($album);
  }

  function search(AlbumSearchParam $album, array $returnedKeys): AlbumAggregate
  {
    return $this->albumRepository->search($album, $returnedKeys);
  }
}
