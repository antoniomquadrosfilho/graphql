<?php

namespace App\Domain\Entities;

class Music
{
  public function __construct(private string $name, private string $file, private Lyric $lyric)
  {
  }

  function toArray(): array
  {
    return [
      'name' => $this->name,
      'file' => $this->file,
      'lyric' => $this->lyric->toArray(),
    ];
  }
}
