<?php

namespace App\Domain\Entities;

class Album
{
  private MusicCollection $musics;

  public function __construct(
    private int $id,
    private string $name,
    private string $artist,
    private float $duration,
  ) {
    $this->musics = new MusicCollection();
  }

  function addMusic(Music $music): void
  {
    $this->musics[] = $music;
  }

  function getMusics(): MusicCollection
  {
    return $this->musics;
  }

  function toArray(): array
  {
    return [
      'id' => $this->id,
      'name' => $this->name,
      'artist' => $this->artist,
      'duration' => $this->duration,
      'musics' => $this->musics,
    ];
  }
}
