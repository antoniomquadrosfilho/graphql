<?php

namespace App\Domain\Entities;

use ArrayObject;
use InvalidArgumentException;

class MusicCollection extends ArrayObject
{
  public function offsetSet($index, $newval): void
  {
    if (!$newval instanceof Music) {
      throw new InvalidArgumentException('Only Music objects can be added to the MusicCollection.');
    }

    parent::offsetSet($index, $newval);
  }
}
