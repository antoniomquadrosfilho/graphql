<?php

namespace App\Domain\Entities;

class Lyric
{
  public function __construct(private string $text)
  {
  }

  function toArray(): array
  {
    return [
      'text' => $this->text,
    ];
  }
}
