<?php

namespace App\Domain\Entities\ValueObjects;

class AlbumSearchParam
{
  public function __construct(
    private ?int $id = null,
    private ?string $name = null,
  ) {
  }

  public function getId(): ?int
  {
    return $this->id;
  }

  public function getName(): ?string
  {
    return $this->name;
  }
}
