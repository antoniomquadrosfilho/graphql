<?php

namespace App\Providers;

use App\Domain\Services\Repositories\AlbumRepository;
use Database\Repositories\AlbumRepository as RepositoriesAlbumRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AlbumRepository::class, RepositoriesAlbumRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
