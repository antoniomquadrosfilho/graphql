<?php

namespace GraphQL\Query;

use App\Models\Lyric;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class LyricQuery extends Query
{
    protected $attributes = [
        'name' => 'lyric',
        'model' => Lyric::class,
    ];

    public function type(): \GraphQL\Type\Definition\Type
    {
        return GraphQL::type('Lyric');
    }


    public function args(): array
    {
        return [
            'id' => [
                'type' => Type::id(),
                'description' => 'Identification of the lyric',
            ],
            'album_id' => [
                'type' => Type::id(),
                'description' => 'Identification of related album',
            ],
            'music_id' => [
                'type' => Type::id(),
                'description' => 'Identification of related music',
            ],
            'text' => [
                'type' => Type::string(),
                'description' => 'The lyric',
            ],
            'shouldSkip' => [
                'type' => Type::boolean(),
                'description' => 'Control lyric exhibition',
            ],
        ];
    }

    public function resolve($root, $args)
    {
        return Lyric::all();;
    }
}
