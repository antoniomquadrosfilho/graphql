<?php

namespace GraphQL\Query;

use App\Domain\Entities\ValueObjects\AlbumSearchParam;
use App\Domain\Services\AlbumService;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Arr;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class AlbumQuery extends Query
{
    protected $attributes = [
        'name' => 'album',
    ];

    function __construct(private AlbumService $albumService)
    {
    }

    public function type(): \GraphQL\Type\Definition\Type
    {
        return GraphQL::type('Album');
    }

    public function args(): array
    {
        return [
            'id' => [
                'type' => Type::id(),
                'description' => 'Identification of the album',
            ],
        ];
    }

    public function resolve($root, $args)
    {
        return $this->albumService
            ->search(new AlbumSearchParam(
                Arr::get($args, 'id', null),
                Arr::get($args, 'name', null)
            ), [
                'id',
                'name',
                'artist',
                'duration',
            ])->toArray();
    }
}
