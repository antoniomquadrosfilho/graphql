<?php

namespace Tests\Feature;

use App\Models\Album;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class AlbumQueryTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_the_album_query_returns_successful_response()
    {
        /** @var int */
        $expectedStatus = 200;

        /** @var Array */
        $expectedContent = [
            'data' => [
                'album' => [
                    'id' => "1",
                    'name' => 'Album 1',
                    'artist' => 'Artist 1',
                    'duration' => 2.0,
                ],
            ],
        ];

        /** @var Album */
        Album::create($expectedContent['data']['album']);

        /** @var TestResponse */
        $response = $this->post('/graphql', [
            'query' => '{ 
                album (id: "1") {
                    id,
                    name,
                    artist,
                    duration
                }
            }',
        ]);

        $response->assertStatus($expectedStatus);
        $response->assertContent(json_encode($expectedContent));
    }
}
