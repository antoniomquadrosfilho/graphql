<?php

namespace Database\Repositories;

use App\Domain\Entities\ValueObjects\AlbumSearchParam;
use App\Domain\Services\Aggregates\AlbumAggregate;
use App\Domain\Services\Repositories\AlbumRepository as IAlbumRepository;
use App\Models\Album;
use Illuminate\Database\Eloquent\Builder;
use RuntimeException;

class AlbumRepository implements IAlbumRepository
{
  function create(AlbumAggregate $album): AlbumAggregate
  {
    throw new RuntimeException('Method not implemented');
  }

  function search(AlbumSearchParam $album, array $returnedKeys): AlbumAggregate
  {
    /** @var Builder */
    $query = Album::query();

    if ($album->getId() != null) {
      $query->whereId($album->getId());
    }

    return AlbumAggregate::make(
      $query
        ->select($returnedKeys)
        ->first()
        ->toArray()
    );
  }
}
