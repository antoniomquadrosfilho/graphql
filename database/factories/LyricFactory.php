<?php

namespace Database\Factories;

use App\Models\Album;
use App\Models\Lyric;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Lyric>
 */
class LyricFactory extends Factory
{
    protected $model = Lyric::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'album_id' => function () {
                return Album::factory()->create()->id;
            },
            'text' => $this->faker->text(),
        ];
    }
}
