<?php

namespace Database\Factories;

use App\Models\Album;
use App\Models\Music;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Music>
 */
class MusicFactory extends Factory
{
    protected $model = Music::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'album_id' => function () {
                return Album::factory()->create()->id;
            },
            'file' => $this->faker->url(),
            'name' => 'Track ' . $this->faker->randomDigit(),
        ];
    }
}
