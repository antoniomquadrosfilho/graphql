<?php

namespace Database\Factories;

use App\Models\Album;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class AlbumFactory extends Factory
{
    protected $model = Album::class;

    protected $fillable = [
        'name',
        'artist',
        'duration',
    ];

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => 'Album name ' . $this->faker->randomDigit(),
            'artist' => fake('pt-br')->name(),
            'duration' => $this->faker->randomFloat(1, 2, 4),
        ];
    }
}
