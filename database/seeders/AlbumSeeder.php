<?php

namespace Database\Seeders;

use App\Models\Album;
use App\Models\Lyric;
use App\Models\Music;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AlbumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Album::factory(5)->create()->each(function (Album $album) {
            Music::factory(10)->create([
                'album_id' => $album->id,
            ])->each(function (Music $music) use ($album) {
                Lyric::factory(1)->create([
                    'album_id' => $album->id,
                    'music_id' => $music->id,
                ]);
            });
        });
    }
}
